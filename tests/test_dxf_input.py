# coding=utf-8

from dxf_input import DxfInput

from inkex.tester import ComparisonMixin, TestCase
from inkex.tester.filters import CompareNumericFuzzy

class TestDxfInputBasic(ComparisonMixin, TestCase):

    compare_file = ['io/test_r12.dxf', 'io/test_r14.dxf', 
    # Unit test for https://gitlab.com/inkscape/extensions/-/issues/355
    # The result for arcs currently looks wrong, but it doesn't crash anymore
    'io/dxf_with_arc.dxf']
    compare_filters = [CompareNumericFuzzy()]
    comparisons = [()]
    effect_class = DxfInput

    def _apply_compare_filters(self, data, is_saving=None):
        """Remove the full pathnames"""
        if is_saving is True:
            return data
        data = super()._apply_compare_filters(data)
        return data.replace((self.datadir() + '/').encode('utf-8'), b'')
    
class TestDxfInputTextHeight(ComparisonMixin, TestCase):
    compare_file = ['io/CADTextHeight.dxf']
    compare_filters = [CompareNumericFuzzy()]
    comparisons = [(), ('--textscale=1.411',)]
    effect_class = DxfInput

    def _apply_compare_filters(self, data, is_saving=None):
        """Remove the full pathnames"""
        if is_saving is True:
            return data
        data = super()._apply_compare_filters(data)
        return data.replace((self.datadir() + '/').encode('utf-8'), b'')

